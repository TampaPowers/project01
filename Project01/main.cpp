#include <iostream>
#include <fstream>
#include <string>
#include "weichenrechner.h"
#include "geldautomat.h"
#include "geldautomat3.h"
#include "notenspiegel.h"
#include "punktnote.h"
#include "readme.h"
#include "farben.h"
#include "zinsrechner.h"
#include "zahlenraten.h"
#include "coutcolors.h"
#include "datumscheck.h"
#include "hexadecimaltest.h"
#include "rechner.h"
#include "reihen.h"
#include <windows.h>


using namespace std;


int main()
{
	bool firstrun = true;
	bool exit = 0;
	int selection = 0;
	int wiederhol = 0;
	int color = 7;
	while (exit == 0)
	{
		if (firstrun != true)
		{
			cout << endl << "Zuruek zum Hauptmenue...";
			system("pause >nul");
		}
		system("cls");
		string line;
		ifstream myfile("menu.txt");
		if (myfile.is_open())
		{
			while (getline(myfile, line))
			{
				if ((line.substr(0, 2) != "//") && (line.substr(0, 1) != "#"))
				{
					cout << line << '\n';
				}
				if (line.substr(0, 1) == "#")
				{
					color = stoi(line.substr(1, 3));
					coutcolors_custom(color);
					cout << line.substr(4, 256) << '\n';
					coutcolors_default();
				}
			}
			myfile.close();
		}
		else cout << "Unable to open readme file";

		cout << "Welche Aufgabe soll ausgefuehrt werden? ";
		cin >> selection;
		while (cin.fail())
		{
			cin.clear();
			cin.ignore(256, '\n');
			cout << "Welche Aufgabe soll ausgefuehrt werden? ";
			cin >> selection;
		}

		cout << endl << endl;
		system("cls");
		firstrun = false;
		switch (selection)
		{
		case 100:
			readme();
			break;
		case 0:
			exit = 1;
			cout << endl << "Zum Beenden Enter druecken!" ;
			break;
		case 1:
			datumscheck();
			break;
		case 2:
			weichenrechner();
			break;
		case 3:
			geldautomat();
			break;
		case 4:
			geldautomat3();
			break;
		case 5:
			notenspiegel();
			break;
		case 6:
			punktezunoten();
			break;
		case 7:
			farben();
			break;
		case 8:
			zinsrechner();
			break;
		case 9:
			zahlenraten();
			break;
		case 10:
			hexadecimaltest();
			break;
		case 11:
			rechner();
			break;
		case 12:
			reiheeingabe();
			break;
		default:
			if (wiederhol > 2)
			{
				throw std::exception();
			}
			else
			{
				cout << endl;
				wiederhol++;
				break;
			}
		}
	}
	if (exit != 1)
	{
		cout << "Enter zum fortfahren... ";
	}
	system("pause >nul");
}