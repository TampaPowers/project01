#include <iostream>
#include <string>
#include "coutcolors.h"

string reihe(int zahl)
{
	string ausgabe = "";

	ausgabe.append("Ausgabe fuer ");
	ausgabe.append(to_string(zahl));
	ausgabe.append(" wird erzeugt:\n");

	for (int i = 0; (i*zahl) <= 100; i++)
	{
		ausgabe.append("\n ");
		ausgabe.append(to_string(i));
		ausgabe.append(" mal ");
		ausgabe.append(to_string(zahl));
		ausgabe.append(" ist gleich ");
		ausgabe.append(to_string(i * zahl));
		ausgabe.append("\n");
	}

	return ausgabe;
}

void reiheeingabe()
{
	string output;
	int zahl;
	cout << "Zu welcher Zahl soll eine Reihe ausgegeben werden? ";
	cin >> zahl;

	while (cin.fail())
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Zu welcher Zahl soll eine Reihe ausgegeben werden? ";
		cin >> zahl;
	}

	output = reihe(zahl);
	cout << endl << output;
}