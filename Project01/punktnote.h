#include <iostream>
#include <string>

using namespace std;

void rechner(int maxpoints, int points)
{
	float note1 = (((float)maxpoints / 100) * 100);
	float note1m = (((float)maxpoints / 100) * 95);

	float note2p = (((float)maxpoints / 100) * 90);
	float note2 = (((float)maxpoints / 100) * 85);
	float note2m = (((float)maxpoints / 100) * 80);

	float note3p = (((float)maxpoints / 100) * 75);
	float note3 = (((float)maxpoints / 100) * 70);
	float note3m = (((float)maxpoints / 100) * 65);

	float note4p = (((float)maxpoints / 100) * 60);
	float note4 = (((float)maxpoints / 100) * 55);
	float note4m = (((float)maxpoints / 100) * 50);

	float note5p = (((float)maxpoints / 100) * 45);
	float note5 = (((float)maxpoints / 100) * 40);
	float note5m = (((float)maxpoints / 100) * 35);

	cout << "Notenverteilung:" << endl << endl;

	cout << "Note 1+ ueber " << maxpoints << " Punkten" << endl;
	cout << "Note 1  ab " << note1 << " Punkten" << endl;
	cout << "Note 1- ab " << note1m << " Punkten" << endl;
	cout << "Note 2+ ab " << note2p << " Punkten" << endl;
	cout << "Note 2  ab " << note2 << " Punkten" << endl;
	cout << "Note 2- ab " << note2m << " Punkten" << endl;
	cout << "Note 3+ ab " << note3p << " Punkten" << endl;
	cout << "Note 3  ab " << note3 << " Punkten" << endl;
	cout << "Note 3- ab " << note3m << " Punkten" << endl;
	cout << "Note 4+ ab " << note4p << " Punkten" << endl;
	cout << "Note 4  ab " << note4 << " Punkten" << endl;
	cout << "Note 4- ab " << note4m << " Punkten" << endl;
	cout << "Note 5+ ab " << note5p << " Punkten" << endl;
	cout << "Note 5  ab " << note5 << " Punkten" << endl;
	cout << "Note 5- ab " << note5m << " Punkten" << endl;

	string endnote;

	if (points < note5m)
	{
		endnote = "6";
	}
	else if (points == note5m || points < note5)
	{
		endnote = "5-";
	}
	else if (points == note5 || points < note5p)
	{
		endnote = "5";
	}
	else if (points == note5p || points < note4m)
	{
		endnote = "5+";
	}
	else if (points == note4m || points < note4)
	{
		endnote = "4-";
	}
	else if (points == note4 || points < note4p)
	{
		endnote = "4";
	}
	else if (points == note4p || points < note3m)
	{
		endnote = "4+";
	}
	else if (points == note3m || points < note3)
	{
		endnote = "3-";
	}
	else if (points == note3 || points < note3p)
	{
		endnote = "3";
	}
	else if (points == note3p || points < note2m)
	{
		endnote = "3+";
	}
	else if (points == note2m || points < note2)
	{
		endnote = "2-";
	}
	else if (points == note2 || points < note2p)
	{
		endnote = "2";
	}
	else if (points == note2p || points < note1m)
	{
		endnote = "2+";
	}
	else if (points == note1m || points < note1)
	{
		endnote = "1-";
	}
	else if (points == note1 || points < maxpoints)
	{
		endnote = "1";
	}
	else if (points == maxpoints || points > maxpoints)
	{
		endnote = "1+";
	}
	else
	{
		cout << "Error cannot computer";
	}

	cout << endl << "Bei " << points << " von " << maxpoints << " Punkten ist die Note: " << endnote << " zu vergeben." << endl << endl;
}

void punktezunoten()
{
	int maxpoints;
	int points;

	cout << "Wieviele Punkt sind maximal zu vergeben: ";
	cin >> maxpoints;
	while (cin.fail())
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Wieviele Punkt sind maximal zu vergeben: ";
		cin >> maxpoints;
	}
	cout << endl;

	cout << "Wieviele Punkte wurden erreicht: ";
	cin >> points;
	while (cin.fail())
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Wieviele Punkte wurden erreicht: ";
		cin >> points;
	}
	while (points > (maxpoints + 10))
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Zu hoher Wert!" << endl;
		cout << "Wieviele Punkte wurden erreicht: ";
		cin >> points;
	}
	cout << endl;

	rechner(maxpoints, points);
}