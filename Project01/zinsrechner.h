#include <iostream>

using namespace std;

void zinsrechner()
{
	float startbetrag;
	float kontostand;
	float zinssatz;
	float entnahme;
	float zinsen;


	do
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Wie viel Geld sind momentan auf dem Konto: ";
		cin >> startbetrag;
		kontostand = startbetrag;

	} while (cin.fail());

	do
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Wie hoch ist die Verzinsung: ";
		cin >> zinssatz;

	} while (cin.fail());

	do
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Wie hoch ist die monatliche Entnahme: ";
		cin >> entnahme;

	} while (cin.fail());

	int monate = 0;
	int jahre = 0;

	if ((kontostand / 100 * zinssatz) > (entnahme * 12))
	{
		cout << endl << "Die Zinsen sind hoeher als die monatliche Entnahme!" << endl;
		cout << "Zinsen belaufen sich auf " << (kontostand / 100 * 4) << " Euro im Jahr" << endl;
		cout << "im Monat sind das " << (kontostand / 100 * 4) / 12 << " Euro" << endl;
		cout << "Die Entnahme liegt im Monat nur bei " << entnahme << " Euro" << endl << endl;
	}
	else
	{
		while (kontostand > entnahme)
		{
			kontostand = kontostand - entnahme;
			if ((monate % 12) == true)
			{
				zinsen = (kontostand / 100 * zinssatz);
				kontostand = kontostand + zinsen;
			}
			monate++;
		}

		while (monate > 12)
		{
			jahre++;
			monate = monate - 12;
		}

		cout << endl << "Bei einem Kontostand von " << (int)startbetrag << " Euro" << endl << "und einer monatlichen Entnahme von " << (int)entnahme << " Euro" << endl;
		cout << "mit einer Verzinsung von " << zinssatz << " also " << zinsen << " Euro im Jahr" << endl << "kann fuer " << jahre << " Jahre und " << monate << " Monate entnommen werden" << endl << endl;
	}
}