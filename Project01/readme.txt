//Readmedatei
//Bitte Kommentare immer mit // beginnen, fuer jede Zeile
#12#Aufgabenverwaltungstool
#17#-----------------------------------------------------------------
Dieses Tool vereinfacht das Aufrufen von verschiedenen Aufgaben
Alle Aufgaben werden im Dialog aufgelistet
und koennen ueber die Eingabe der zugehoerigen Kennnummern aufgerufen werden
Mit 100 wird immer dieses Readme angezeigt
Mit 0 wird immer das Tool beendet
#17#-----------------------------------------------------------------

#12#Weichenrechner
#17#-----------------------------------------------------------------
Beim Weichenrechner soll berechnet werden in welchem Ausgang eine Kugel landet.
Die Kugel rollt je nach Stellung der Weichen entweder in Ausgang 0 oder 1.
Die erste Weiche entscheidet ob die Kugel ueber Weiche 2 oder 3 laeuft
Weiche 2 und 3 koennen auf Weiche 4 abgeben welche dann widerum zwischen den
beiden Ausgaengen entscheiden kann.

Die Aufgabe wurde so geloest, dass je nach Stellung der Weichen auch immer nur
die Weiche abgefragt wird welche wirklich relevant ist.
Fuer die richtige Loesung bzw. das Ausrechnen des Ausgang muessen immer nur 
minimal 2 und maximal 3 Weichenstellungen bekannt sein.
#17#-----------------------------------------------------------------

#12#Geldautomat
#17#-----------------------------------------------------------------
Die Aufgabe bestand darin eine Ausgabe von Geldscheinen auszurechnen nachdem
der Benutzer einen auszuzahlenden Betrag angegeben hat.

Die Aufgabe wurde auf drei Arten geloest. Strikt nach Aufgabenstellung.
Zwei weitere Varianten verteilen die Scheine gleichmae�ig. Bei der letzten 
Version wurde der Quellcode so weit wie moeglich verkleinert und Code der sich
wiederholt in Funktionen ausgelagert.
#17#-----------------------------------------------------------------

��

#12#Notenspiegel
#17#-----------------------------------------------------------------
Aufgabe hier war es den durchschnittlichen Notenspiegel zu errechnen.
Die Ausgabe sollte uebersichtlich strukturiert werden und sich der Eingabe
anpassen.

Die Aufgabe wurde durch mit simpelen IF-Verzweigungen geloest, alternativ
koennte man auch die Laenge der Ausgabe abfragen und dementsprechend anspassen.
#17#-----------------------------------------------------------------

#12#Punkte zu Noten
#17#-----------------------------------------------------------------
Aufgabe bestand darin einen Rechner zu erstellen welcher die erreichten Punkte
in Noten umwandelt. Der verwendete Schluessel sollte hier mit ausgegeben werden.

Die Aufgabe wurde in zwei Funktionen aufgeteilt um Wiederholungen zu vermeiden
Die maximale Punktzahl kann eingegeben werden und die Note wird anhand der
erriechten Punkte ausgerechnet. Ein anpassen des Schluessels ist nicht moeglich.
#17#-----------------------------------------------------------------

#12#Konsolenfarben
#17#-----------------------------------------------------------------
Dient eigentlich nur als Experiment fuer die verschiedenen Farben die die
Konsole annehmen kann.
#17#-----------------------------------------------------------------

#12#Auskommensrechner
#17#-----------------------------------------------------------------
Falls man mal im Lotto gewinnt oder einfach nur seine Rente planen moechte.
Hier sollte errechnet werden wie lange man Geld von seinem Konto nehmen kann

Nach der Eingabe des Startkapitals, Zinsen und der monatlichen Entnahme werden
die Monate errechnet bis der Kontostand nicht mehr die Entnahme deckt.
#17#-----------------------------------------------------------------

��

#12#Zahlenraten
#17#-----------------------------------------------------------------
Generiert 6 unterschiedliche Zufallszahlen von 1 bis 49
Danach werden fuer jede Zahl so lange Zahlen generiert bis die Zufallszahl
mit der generierten Zahl �bereinstimmt.
#17#-----------------------------------------------------------------

#12#Hexaldezimaluebungen
#17#-----------------------------------------------------------------
Zum Erlernen des Hexaldezimalsystems a l� Vokabeltest
Einfach gehalten, generiert die Bitfolge willkuerlich und fragt dann anhand
einer Tabelle ab welche Hexalzahl dazu passt, fragt so lange bist stop
eingeben wird.
#17#-----------------------------------------------------------------



#17#-----------------------------------------------------------------
#12#Geschrieben von Vincent Sylvester
#17#-----------------------------------------------------------------