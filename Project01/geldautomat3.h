#include <iostream>
#include "coutcolors.h"

using namespace std;

int scheine(int betrag, int& berechnet, int scheinsize, int scheinnumber)
{
	if ((betrag - berechnet) > (scheinsize - 1))
	{
		scheinnumber++;
		berechnet = berechnet + scheinsize;
	}
	return scheinnumber;
	return berechnet;
}

void geldautomat3()
{
	int betrag;
	int scheinsize = 0;
	int berechnet = 0;
	int scheine500 = 0;
	int scheine200 = 0;
	int scheine100 = 0;
	int scheine50 = 0;
	int scheine20 = 0;
	int scheine10 = 0;
	int scheine5 = 0;
	float test;
	cout << "Wieviel Geld soll ausgezahlt werden?: ";
	cin >> betrag;

	test = betrag / 5;

	if ((((int)test * 5) == betrag) && (betrag > 5))
	{
		while (berechnet < betrag)
		{
			scheinsize = 500;
			scheine500 = scheine(betrag, berechnet, 500, scheine500);
			scheinsize = 200;
			scheine200 = scheine(betrag, berechnet, 200, scheine200);
			scheinsize = 100;
			scheine100 = scheine(betrag, berechnet, 100, scheine100);
			scheinsize = 50;
			scheine50 = scheine(betrag, berechnet, 50, scheine50);
			scheinsize = 20;
			scheine20 = scheine(betrag, berechnet, 20, scheine20);
			scheinsize = 10;
			scheine10 = scheine(betrag, berechnet, 10, scheine10);
			scheinsize = 5;
			scheine5 = scheine(betrag, berechnet, 5, scheine5);
		}

		cout << "============= Ausgabe =============" << endl;
		coutcolors_green();

		cout << scheine500;
		if (scheine500 < 100){ cout << "   "; }
		else if (scheine500 < 10){ cout << "  "; }
		else{ cout << " "; }
		cout << " x   500 Euro Scheine" << endl;

		cout << scheine200;
		if (scheine200 < 100){ cout << "   "; }
		else if (scheine200 < 10){ cout << "  "; }
		else{ cout << " "; }
		cout << " x   200 Euro Scheine" << endl;

		cout << scheine100;
		if (scheine100 < 100){ cout << "   "; }
		else if (scheine100 < 10){ cout << "  "; }
		else{ cout << " "; }
		cout << " x   100 Euro Scheine" << endl;

		cout << scheine50;
		if (scheine50 < 100){ cout << "   "; }
		else if (scheine50 < 10){ cout << "  "; }
		else{ cout << " "; }
		cout << " x    50 Euro Scheine" << endl;

		cout << scheine20;
		if (scheine20 < 100){ cout << "   "; }
		else if (scheine20 < 10){ cout << "  "; }
		else{ cout << " "; }
		cout << " x    20 Euro Scheine" << endl;

		cout << scheine10;
		if (scheine10 < 100){ cout << "   "; }
		else if (scheine10 < 10){ cout << "  "; }
		else{ cout << " "; }
		cout << " x    10 Euro Scheine" << endl;

		cout << scheine5;
		if (scheine5 < 100){ cout << "   "; }
		else if (scheine5 < 10){ cout << "  "; }
		else{ cout << " "; }
		cout << " x     5 Euro Scheine" << endl;

		coutcolors_default();
		cout << "===================================" << endl;
	}
	else
	{
		cout << "Geldautomat kann nur in fuenferschritten ausgeben!" << endl;
	}
}