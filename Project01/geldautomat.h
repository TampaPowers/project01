#include <iostream>

using namespace std;

void geldautomat()
{
	int betrag;
	int zwischensumme1 = 0;
	int zwischensumme2 = 0;
	int zwischensumme3 = 0;
	int zwischensumme4 = 0;
	int zwischensumme5 = 0;
	int zwischensumme6 = 0;
	int zwischensumme7 = 0;
	int rest1 = 0;
	int rest2 = 0;
	int rest3 = 0;
	int rest4 = 0;
	int rest5 = 0;
	int rest6 = 0;
	int scheine500 = 0;
	int scheine200 = 0;
	int scheine100 = 0;
	int scheine50 = 0;
	int scheine20 = 0;
	int scheine10 = 0;
	int scheine5 = 0;
	float test;
	cout << "Wieviel Geld soll ausgezahlt werden?: ";
	cin >> betrag;

	test = betrag / 5;

	if (((int)test * 5) == betrag && betrag > 5)
	{
		while ((zwischensumme1 + 500) < (betrag + 1))
		{
			++scheine500;
			zwischensumme1 = scheine500 * 500;
		}
		rest1 = betrag - zwischensumme1;

		while ((zwischensumme2 + 200) < (rest1 + 1))
		{
			++scheine200;
			zwischensumme2 = scheine200 * 200;
		}
		rest2 = rest1 - zwischensumme2;

		while ((zwischensumme3 + 100) < (rest2 + 1))
		{
			++scheine100;
			zwischensumme3 = scheine100 * 100;
		}
		rest3 = rest2 - zwischensumme3;

		while ((zwischensumme4 + 50) < (rest3 + 1))
		{
			++scheine50;
			zwischensumme4 = scheine50 * 50;
		}
		rest4 = rest3 - zwischensumme4;

		while ((zwischensumme5 + 20) < (rest4 + 1))
		{
			++scheine20;
			zwischensumme5 = scheine20 * 20;
		}
		rest5 = rest4 - zwischensumme5;

		while ((zwischensumme6 + 10) < (rest5 + 1))
		{
			++scheine10;
			zwischensumme6 = scheine10 * 10;
		}
		rest6 = rest5 - zwischensumme6;

		while ((zwischensumme7 + 5) < (rest6 + 1))
		{
			++scheine5;
			zwischensumme7 = scheine5 * 5;
		}

		cout << "============= Ausgabe =============" << endl;
		cout << scheine500 << " x 500 Euro Scheine" << endl;
		cout << scheine200 << " x 200 Euro Scheine" << endl;
		cout << scheine100 << " x 100 Euro Scheine" << endl;
		cout << scheine50 << " x 50 Euro Scheine" << endl;
		cout << scheine20 << " x 20 Euro Scheine" << endl;
		cout << scheine10 << " x 10 Euro Scheine" << endl;
		cout << scheine5 << " x 5 Euro Scheine" << endl;
		cout << "===================================" << endl;
	}
	else
	{
		cout << "Geldautomat kann nur in fuenferschritten ausgeben!" << endl;
	}
}