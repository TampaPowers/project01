#include "coutcolors.h"
#include <iostream>
#include <string>

using namespace std;

void datumscheck()
{
	string datum;
	int tag;
	int monat;
	int jahr;
	bool schaltjahr = false;
	bool korrekt = false;
	cout << "Datum angeben[TT.MM.JJJJ]: ";
	cin >> datum;

	sscanf_s(datum.c_str(), "%2d.%2d.%4d",
		&tag,
		&monat,
		&jahr);

	if (((tag > 0) && (tag < 32)) && ((monat > 0) && (monat < 13)) && ((jahr > 0) && (jahr < 10000)))
	{
		cout << "Eingegebenes Datum: " << tag << "." << monat << "." << jahr << endl;

		if (jahr % 4 == 0)
		{
			if (jahr % 100)
			{
				if (jahr % 400 == 0)
				{
					schaltjahr = true;
				}
				else
				{
					schaltjahr = false;
				}
			}
			else
			{
				schaltjahr = true;
			}
		}
		else
		{
			schaltjahr = false;
		}
	}
	else
	{
		cout << "Datum nicht korrekt!" << endl;
		return;
	}

	if (schaltjahr == true)
	{
		cout << "Datum ist in einem Schaltjahr!" << endl;
	}
	else
	{
		cout << "Datum ist nicht in einem Schaltjahr!" << endl;
	}

	if (monat == 1 || monat == 3 || monat == 5 || monat == 7 || monat == 8 || monat == 10 || monat == 12)
	{
		if (tag < 32)
		{
			korrekt = true;
		}
		else
		{
			korrekt = false;
		}

	}
	else if (monat == 2)
	{
		if ((schaltjahr == true) && (tag < 30))
		{
			korrekt = true;
		}
		else if ((schaltjahr == false) && (tag < 29))
		{
			korrekt = true;
		}
		else
		{
			korrekt = false;
		}

	}
	else
	{
		if (tag < 31)
		{
			korrekt = true;
		}
		else
		{
			korrekt = false;
		}
	}

	if (korrekt == true)
	{
		cout << "Datum korrekt!" << endl;
	}
	else
	{
		cout << "Datum nicht korrekt!" << endl;
	}
}