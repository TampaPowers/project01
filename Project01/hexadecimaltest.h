#include <iostream>
#include <string>
#include <cstdlib>
#include "coutcolors.h"

using namespace std;

void hexadecimaltest()
{
	coutcolors_red();
	cout << "Hexaldezimal Uebungen, zum aufh�ren \"stop\" eingeben" << endl << endl;
	int exit = 0;
	coutcolors_default();

	while (exit == 0)
	{
		int bit1 = rand() % 2 + 0;
		int bit2 = rand() % 2 + 0;
		int bit3 = rand() % 2 + 0;
		int bit4 = rand() % 2 + 0;
		string bit = to_string(bit1) + to_string(bit2) + to_string(bit3) + to_string(bit4);
		string hex;
		string input;

		if (bit == "0000")
		{
			hex = "0";
		}
		else if (bit == "0001")
		{
			hex = "1";
		}
		else if (bit == "0010")
		{
			hex = "2";
		}
		else if (bit == "0011")
		{
			hex = "3";
		}
		else if (bit == "0100")
		{
			hex = "4";
		}
		else if (bit == "0101")
		{
			hex = "5";
		}
		else if (bit == "0110")
		{
			hex = "6";
		}
		else if (bit == "0111")
		{
			hex = "7";
		}
		else if (bit == "1000")
		{
			hex = "8";
		}
		else if (bit == "1001")
		{
			hex = "9";
		}
		else if (bit == "1010")
		{
			hex = "a";
		}
		else if (bit == "1011")
		{
			hex = "b";
		}
		else if (bit == "1100")
		{
			hex = "c";
		}
		else if (bit == "1101")
		{
			hex = "d";
		}
		else if (bit == "1110")
		{
			hex = "e";
		}
		else if (bit == "1111")
		{
			hex = "f";
		}

		cout << "Welches Hexalzeichen passt zu: " << bit << " ? ";
		cin >> input;

		if (input == hex)
		{
			coutcolors_green();
			cout << "Richtig! " << bit << " ist " << hex << " in Hexaldezimal.";
			Sleep(1500);
			coutcolors_default();
		}
		else if (input == "stop")
		{
			exit = 1;
		}
		else
		{
			coutcolors_red();
			cout << "Leider falsch! Richtig ist " << hex;
			Sleep(1500);
			coutcolors_default();
		}
		system("cls");
		coutcolors_red();
		cout << "Hexaldezimal Uebungen, zum aufh�ren \"stop\" eingeben" << endl << endl;
		coutcolors_default();
	}
}