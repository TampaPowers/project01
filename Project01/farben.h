#include <iostream>
#include <windows.h>
#include "coutcolors.h"

using namespace std;

void SetColor(int value){
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), value);
}

void farben()
{
	for (int i = 0; i < 256; i++)
	{
		coutcolors_default();
		cout << "Momentaner Farbencode: " << i << " ";
		coutcolors_custom(i);
		cout << "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789         " << endl;
	}

	coutcolors_default();
}