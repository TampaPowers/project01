#include <iostream>
#include <string>
#include <xtr1common>
#include "coutcolors.h"

using namespace std;

float opminus(float a, float b)
{
	float c = a - b;
	return c;
}
float opplus(float a, float b)
{
	float c = a + b;
	return c;
}
float opmal(float a, float b)
{
	float c = a * b;
	return c;
}
float opteilt(float a, float b)
{
	float c = a / b;
	return c;
}


void rechner()
{
	string eingabe;
	int count1 = 0;
	int count2 = 0;
	float result = 0;
	float a;
	float b;

	coutcolors_red();
	cout << "Geben sie die Gleichung ein: " << endl << endl;
	coutcolors_default();
	cin >> eingabe;


	while (eingabe.substr(count1, 1) != "-" && eingabe.substr(count1, 1) != "+" && eingabe.substr(count1, 1) != "*" && eingabe.substr(count1, 1) != "/")
	{
		count1++;
	}	
	a = stof(eingabe.substr(0, count1));

	count2 = count1 + 1;

	while (eingabe.substr(count2, 1) != "")
	{
		count2++;
	}
	b = stof(eingabe.substr((count1 + 1), count2));

	if (eingabe.substr((count1), 1) == "-")
	{
		result = opminus(a, b);
	}
	else if (eingabe.substr((count1), 1) == "+")
	{
		result = opplus(a, b);
	}
	else if (eingabe.substr((count1), 1) == "*")
	{
		result = opmal(a, b);
	}
	else if (eingabe.substr((count1), 1) == "/")
	{
		result = opteilt(a, b);
	}

	coutcolors_green();
	cout << endl << eingabe << " ergibt: " << result << endl;
	coutcolors_default();
}