#include <iostream>
#include <cstdlib>
#include "windows.h"
#include "coutcolors.h"

using namespace std;

void zahlenraten()
{
	int randzahl1 = 0;
	int randzahl2 = 0;
	int randzahl3 = 0;
	int randzahl4 = 0;
	int randzahl5 = 0;
	int randzahl6 = 0;

	int rechzahl1 = 0;
	int rechzahl2 = 0;
	int rechzahl3 = 0;
	int rechzahl4 = 0;
	int rechzahl5 = 0;
	int rechzahl6 = 0;

	int zahl = 0;

	randzahl1 = rand() % 49 + 1;
	randzahl2 = rand() % 49 + 1;
	randzahl3 = rand() % 49 + 1;
	randzahl4 = rand() % 49 + 1;
	randzahl5 = rand() % 49 + 1;
	randzahl6 = rand() % 49 + 1;

	for (int i = 0; i < 7; i++)
	{

		zahl = randzahl1;
		while (zahl == randzahl2 || zahl == randzahl3 || zahl == randzahl4 || zahl == randzahl5 || zahl == randzahl6)
		{
			randzahl1 = rand() % 49 + 1;
		}

		zahl = randzahl2;
		while (zahl == randzahl1 || zahl == randzahl3 || zahl == randzahl4 || zahl == randzahl5 || zahl == randzahl6)
		{
			randzahl2 = rand() % 49 + 1;
		}

		zahl = randzahl3;
		while (zahl == randzahl1 || zahl == randzahl2 || zahl == randzahl4 || zahl == randzahl5 || zahl == randzahl6)
		{
			randzahl3 = rand() % 49 + 1;
		}

		zahl = randzahl4;
		while (zahl == randzahl1 || zahl == randzahl2 || zahl == randzahl3 || zahl == randzahl5 || zahl == randzahl6)
		{
			randzahl4 = rand() % 49 + 1;
		}

		zahl = randzahl5;
		while (zahl == randzahl1 || zahl == randzahl2 || zahl == randzahl3 || zahl == randzahl4 || zahl == randzahl6)
		{
			randzahl5 = rand() % 49 + 1;
		}

		zahl = randzahl6;
		while (zahl == randzahl1 || zahl == randzahl2 || zahl == randzahl3 || zahl == randzahl4 || zahl == randzahl5 )
		{
			randzahl6 = rand() % 49 + 1;
		}

	}

	cout << "Die erste   Zufallszahl ist:  " << randzahl1 << endl;
	cout << "Die zweite  Zufallszahl ist:  " << randzahl2 << endl;
	cout << "Die dritte  Zufallszahl ist:  " << randzahl3 << endl;
	cout << "Die vierte  Zufallszahl ist:  " << randzahl4 << endl;
	cout << "Die fuenfte Zufallszahl ist:  " << randzahl5 << endl;
	cout << "Die sechste Zufallszahl ist:  " << randzahl6 << endl;

	cout << endl;

	coutcolors_red();
	cout << "Fuer die erste Zahl mussten folgende Zahlen generiert werden: " << endl << endl;
	coutcolors_default();
	do
	{
		rechzahl1 = rand() % 49 + 1;
		cout << rechzahl1 << " ";
	} while (randzahl1 != rechzahl1);
	coutcolors_red();
	cout << endl << endl << randzahl1 << " == " << rechzahl1 << endl;
	coutcolors_default();
	cout << endl << endl;

	coutcolors_red();
	cout << "Fuer die zweite Zahl mussten folgende Zahlen generiert werden: " << endl << endl;
	coutcolors_default();
	do
	{
		rechzahl2 = rand() % 49 + 1;
		cout << rechzahl2 << " ";
	} while (randzahl2 != rechzahl2);
	coutcolors_red();
	cout << endl << endl << randzahl2 << " == " << rechzahl2 << endl;
	coutcolors_default();
	cout << endl << endl;

	coutcolors_red();
	cout << "Fuer die dritte Zahl mussten folgende Zahlen generiert werden: " << endl << endl;
	coutcolors_default();
	do
	{
		rechzahl3 = rand() % 49 + 1;
		cout << rechzahl3 << " ";
	} while (randzahl3 != rechzahl3);
	coutcolors_red();
	cout << endl << endl << randzahl3 << " == " << rechzahl3 << endl;
	coutcolors_default();
	cout << endl << endl;

	coutcolors_red();
	cout << "Fuer die vierte Zahl mussten folgende Zahlen generiert werden: " << endl << endl;
	coutcolors_default();
	do
	{
		rechzahl4 = rand() % 49 + 1;
		cout << rechzahl4 << " ";
	} while (randzahl4 != rechzahl4);
	coutcolors_red();
	cout << endl << endl << randzahl4 << " == " << rechzahl4 << endl;
	coutcolors_default();
	cout << endl << endl;

	coutcolors_red();
	cout << "Fuer die fuenfte Zahl mussten folgende Zahlen generiert werden: " << endl << endl;
	coutcolors_default();
	do
	{
		rechzahl5 = rand() % 49 + 1;
		cout << rechzahl5 << " ";
	} while (randzahl5 != rechzahl5);
	coutcolors_red();
	cout << endl << endl << randzahl5 << " == " << rechzahl5 << endl;
	coutcolors_default();
	cout << endl << endl;

	coutcolors_red();
	cout << "Fuer die sechste Zahl mussten folgende Zahlen generiert werden: " << endl << endl;
	coutcolors_default();
	do
	{
		rechzahl6 = rand() % 49 + 1;
		cout << rechzahl6 << " ";
	} while (randzahl6 != rechzahl6);
	coutcolors_red();
	cout << endl << endl << randzahl6 << " == " << rechzahl6 << endl;
	coutcolors_default();
	cout << endl << endl;
}