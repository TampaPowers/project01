#include <iostream>
#include <fstream>
#include <string>
#include "windows.h"
#include "coutcolors.h"

using namespace std;

void readme()
{
	int color = 7;
	string line;
	ifstream myfile("readme.txt");
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			if ((line.substr(0, 2) != "//") && (line.substr(0, 1) != "#") && (line.substr(0, 2) != "��"))
			{
				cout << line << '\n';
			}
			if (line.substr(0, 1) == "#")
			{
				color = stoi(line.substr(1, 3));
				coutcolors_custom(color);
				cout << line.substr(4, 256) << '\n';
				coutcolors_default();
			}
			if (line.substr(0, 2) == "��")
			{
				cout << endl << "Enter zum fortfahren...";
				system("pause >nul");
				system("cls");
			}
		}
		myfile.close();
		cout << endl << endl;
	}
	else cout << "Unable to open readme file";
}