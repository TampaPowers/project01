#include <iostream>

using namespace std;

void output(int tooutput)
{
	if (tooutput > 9)
	{
		cout << "   " << tooutput << "    ";
	}
	else
	{
		cout << "    " << tooutput << "    ";
	}
	cout << "|";
}

void notenspiegel()
{
	int einsen;
	int zweien;
	int dreien;
	int vieren;
	int fuenfen;
	int sechsen;

	float spiegel;

	do
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Anzahl an Einsen: ";
		cin >> einsen;
	} while (cin.fail());
	
	do
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Anzahl an Zweien: ";
		cin >> zweien;
	} while (cin.fail());
	
	do
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Anzahl an Dreien: ";
		cin >> dreien;
	} while (cin.fail());
	
	do
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Anzahl an Vieren: ";
		cin >> vieren;
	} while (cin.fail());
	
	do
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Anzahl an Fuenfen: ";
		cin >> fuenfen;
	} while (cin.fail());
	
	do
	{
		cin.clear();
		cin.ignore(256, '\n');
		cout << "Anzahl an Sechsen: ";
		cin >> sechsen;
	} while (cin.fail());
	
	spiegel = (((float)einsen)+((float)zweien * 2) + ((float)dreien * 3) + ((float)vieren * 4) + ((float)fuenfen * 5) + ((float)sechsen * 6)) / ((float)einsen + (float)zweien + (float)dreien + (float)vieren + (float)fuenfen + (float)sechsen);
	
	cout << endl << "| Einsen  | Zweien  | Dreien  | Vieren  | Fuenfen | Sechsen |" << endl;
	cout << "-------------------------------------------------------------------------" << endl;
	cout << "|";
	output(einsen);
	output(zweien);
	output(dreien);
	output(vieren);
	output(fuenfen);
	output(sechsen);
	cout << endl << endl << "Der durchschnittliche Notenspiegel liegt bei: " << spiegel << endl << endl << endl;;
}